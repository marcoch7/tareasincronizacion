#include "part2.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include<pthread.h>


int lifespan = 0;             //the lifespan of a turtle
int chance_reproduce = 0;     //the probability of a turtle generating an offspring each tick
int carrying_capacity = 0;   //the number of turtles that can be in the world at one time
int immunity_duration = 0;
int sick_q = 0;
int count_turtles = 0;
int week = 0;
int died = 0;
float p_infected = 0;
float p_immune = 0;
float p_healthy = 0;
int rand_move = 0;

/*Matrix to store the humans*/
patch mat[DIM][DIM];

/*Mutex object to synchronize the threads*/
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void fill_mat(){
    /*Fill th matrix with NULL pointer to human and a not occupied patch*/
    for(int i = 0; i < DIM; i++)
    {
        for (int j = 0; j < DIM; j++)
        {
            mat[i][j].humano = NULL;
            mat[i][j].occupied = false;
        }
            
    }
}


human* human_data(){
    /*Allocate space for a human in memory*/
    human* humanP = malloc(sizeof(human));
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;
    humanP->age = 0;
    return humanP;
}



void matriz_human(int number_people){
    /*Generate random numbers with time as seed*/
    srand(time(NULL));
    for(int i = 0; i < number_people; i++){
        /*Generate random x,y coordinate*/
        int x = rand()%DIM;
        int y = rand()%DIM;
        /*Coordinates must not go to an occupied field*/
        while (mat[x][y].occupied == 1)
        {
            x = rand()%DIM;
            y = rand()%DIM;
        }
        mat[x][y].humano = human_data();
        mat[x][y].occupied = true;
        mat[x][y].moved = false;
        /* Give initial values for human parameters*/
        setup_turtles(mat[x][y].humano, number_people);
    }
}

void print_mat(){
    /*Print the matriz showing tha age of each human*/
    for(int i = 0; i < DIM; i++){
        for(int j = 0; j < DIM; j++){
            if(mat[i][j].humano != NULL){
                printf("%p, %d |", mat[i][j].humano, mat[i][j].humano->age);
            }
            else
            {
                printf("%d |", 0);
            }
        }
        printf("\n");
    }
}

results* results_data(){
    /*Allocate space for results in memory*/
    results* resultsP = malloc(sizeof(results));
    resultsP->infected = 0;
    resultsP->immune = 0;
    resultsP->healthy = 0;
    return resultsP;
}

void setup_constants(){
    srand(time(NULL));
    lifespan = 2600; 
    carrying_capacity = 300;
    chance_reproduce = 1;
    immunity_duration = 52;
}

void get_sick(human* humanP){
    humanP->sick = 1;
    humanP->remaining_inmunity = 0;
    //printf("SE ENFERMO %d\n", humanP->age);
}

void get_healthy(human* humanP){
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;  
}

void become_immune(human* humanP){
    humanP->sick = 0;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = immunity_duration;
}

void get_older(int x, int y){
    died = 0;
    /*Age increases by 1*/
    mat[x][y].humano->age++;
    /*Check if human is old enough to die*/
    if(mat[x][y].humano->age > lifespan){
        died = 1;
        mat[x][y].humano->age = 0;
        mat[x][y].humano->remaining_inmunity = 0;
        mat[x][y].humano->sick = 0;
        mat[x][y].humano->sick_time = 0;
        mat[x][y].moved = 0;
        mat[x][y].occupied = 0;
        count_turtles--;
    } 
    /*If human did not die*/
    if(!died && mat[x][y].humano != NULL){
        /*If still immune,, reduce immunity*/
        if(mat[x][y].humano->remaining_inmunity > 0){
            mat[x][y].humano->remaining_inmunity--;
        }
        /*If sick increase sick time*/
        if(mat[x][y].humano->sick){
            mat[x][y].humano->sick_time++;
        }
    }    
}

void recover_or_die(int duration, int chance_recover, int x, int y){
    if((mat[x][y].humano != NULL)){
        /*If human has been sick for longer than the duration of the virus*/
        if(mat[x][y].humano->sick_time > duration){
            /*Human will become immune*/
            if((rand() % 100) < chance_recover){
                become_immune(mat[x][y].humano);
                //printf("un become_immune\n");
            }
            /*Or human will die*/
            else{
                mat[x][y].humano->age = 0;
                mat[x][y].humano->remaining_inmunity = 0;
                mat[x][y].humano->sick = 0;
                mat[x][y].humano->sick_time = 0;
                mat[x][y].moved = 0;
                mat[x][y].occupied = 0;
                died = 1;
                count_turtles--;
            }
        }
    }    
}


void* update_global_variables(void *arg){
    results* results = malloc(sizeof(results));
    limits * lim = (limits *)(arg);
    float c_infected = 0;
    float c_immune = 0;
    float c_healthy = 0;
    int occu = 0;
    /*If there is still people alive*/
    if(lim->number_people > 0){
        /*Check a portion of the matrix and count infected, immune and healthy humans*/
        for (int i = lim->x_inf; i < lim->x_sup; i++)
        {
            for (int j = lim->y_inf; j < lim->y_sup; j++)
            {
                if(mat[i][j].occupied){  
                    occu++;
                
                    if((mat[i][j].humano->sick)){
                        c_infected++;
                    }
                    if((mat[i][j].humano->remaining_inmunity > 0)){
                        c_immune++;
                    }
                    if((mat[i][j].humano->sick == 0) && (mat[i][j].humano->remaining_inmunity == 0)){
                        c_healthy++;
                    }
                    
                }
            }       
        }      
    }
    /*Lock the global variable to be used by 1 thread at the same time*/
    pthread_mutex_lock(&mutex);
    p_infected = (c_infected/lim->number_people)*100;
    p_immune = (c_immune/lim->number_people)*100;
    p_healthy = (c_healthy/lim->number_people)*100;
    /*Unlock the global variables*/
    pthread_mutex_unlock(&mutex);
    /*Store the results of the portion to be used later*/
    results->infected = p_infected;
    results->immune = p_immune;
    results->healthy = p_healthy;
    pthread_exit(results);
}


void move(int pos[], human* humanP, int i, int j){
    /*Move with case 1-3, or set x = 1*/
    rand_move++;
    if(rand_move == 4){
        rand_move = 0;
    }
    int x = 0;
    int y = 0;
    if((rand_move == 0) && (i+1 < DIM) && (j+1 < DIM)){
        x = i+1; 
        y = j+1;
    }
    else if((rand_move == 1) && (i+1 < DIM) && (j-1 > 0)){
        x = i+1; 
        y = j-1;
        
    }
    else if((rand_move == 2) && (i-1 > 0) && (j+1 < DIM)){
        x = i-1; 
        y = j+1;
    }
    else if((rand_move == 3) && (i-1 > 0) && (j-1 > 0)){
        x = i-1; 
        y = j-1;
    }
    else{
        x = 1;
    }
    /*Keep moving if patch is occupied (2D matrix)*/
    while (mat[x][y].occupied == 1)
    {
        if(x+1 < DIM){
            x++;
        }
        else{
            x = 0;
        }
        if(mat[x][y].occupied == 1){
            if(y+1 < DIM){
                y++;
            }
            else{
                y = 0;
            }
        }

    }
    /*Update human position*/
    mat[x][y].humano = human_data();
    mat[x][y].humano->age = mat[i][j].humano->age;
    mat[x][y].humano->sick = mat[i][j].humano->sick;
    mat[x][y].humano->sick_time = mat[i][j].humano->sick_time;
    mat[x][y].humano->remaining_inmunity = mat[i][j].humano->remaining_inmunity;

    mat[x][y].occupied = true;
    mat[x][y].moved = true;
    mat[i][j].humano = NULL;
    mat[i][j].occupied = 0;
    mat[i][j].moved = 0;
    pos[0] = x;
    pos[1] = y;
}

void infect(human* humanP, int infectiousness, int i, int j){
    for(int x = 0; x < DIM; x++){
        for(int y = 0; y < DIM; y++){
            /* Occupied, Not sick, not immune and adyacent patches*/
            if((mat[x][y].occupied) && !(mat[x][y].humano->sick) && (mat[x][y].humano->remaining_inmunity == 0) && ((x == i+1 && (y == j+1 || y == j-1)) || (x == i-1 && (y == j+1 || y == j-1)))){
                int rand_infect = rand() % 100;
                if(rand_infect < infectiousness){
                    get_sick(mat[x][y].humano);
                }
            }
        }
    }
}

void hatch(){
    /*Generate random x,y coordinate*/
    int x = rand()%DIM;
    int y = rand()%DIM;
    /*Coordinates must not go to and occupied field*/
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    mat[x][y].humano = human_data();
    mat[x][y].occupied = true;
    mat[x][y].moved = false;
    get_healthy(mat[x][y].humano);
    mat[x][y].humano->age = 1;
    count_turtles++;
}

void reproduce(int number_people, int chance_reproduce){
    int rand_reproduce = rand() % 100;
    /*If there is still room for more humans and the chance of reproduce is correct, create a new human*/
    if((count_turtles < carrying_capacity) && (rand_reproduce < chance_reproduce)){
        hatch(mat);
    }
}

void setup_turtles(human* humanP, int number_people){
    int sick = rand() % number_people;
    humanP->age = rand() % lifespan;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = 0;
    get_healthy(humanP);
    /*Probability of being sick for the first humans in the model*/
    if(sick < (number_people/4 + 1)){
        get_sick(humanP);
    }
    else{
        //printf("NOT SICK\n");
    }
}

void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks){
    /*Set initial conditions*/
    setup_constants();
    fill_mat();
    sick_q = 0;
    char* fname = malloc(25*sizeof(char));
    fname = "threads.csv";
    write_file(fname);
    count_turtles = number_people;
    matriz_human(number_people);
    limits a;
    a.x_inf = 0;
    a.y_inf = 0;
    a.x_sup = (DIM/2);
    a.y_sup = (DIM/2);
    a.number_people = count_turtles;
    limits b;
    b.x_inf = DIM/2;
    b.y_inf = 0;
    b.x_sup = DIM;
    b.y_sup = (DIM/2);    
    b.number_people = count_turtles;
    limits c;
    c.x_inf = 0;
    c.y_inf = (DIM/2);
    c.x_sup = (DIM/2);
    c.y_sup = DIM;
    c.number_people = count_turtles;
    limits d;
    d.x_inf = DIM/2;
    d.y_inf = DIM/2;
    d.x_sup = DIM;
    d.y_sup = DIM;
    d.number_people = count_turtles;
    /*Declaration and creation of threads*/
    pthread_t t1;
    pthread_create(&t1,NULL,update_global_variables,&a);
    pthread_t t2;
    pthread_create(&t2,NULL,update_global_variables,&b);
    pthread_t t3;
    pthread_create(&t3,NULL,update_global_variables,&c);
    pthread_t t4;
    pthread_create(&t4,NULL,update_global_variables,&d);
    void* r_av;
    void* r_bv;
    void* r_cv;
    void* r_dv;
    /*Join the threads when completed*/
    pthread_join(t1, &r_av);
    pthread_join(t2, &r_bv);
    pthread_join(t3, &r_cv);
    pthread_join(t4, &r_dv);
    /*get the returned values*/
    results* r_a = (results *)r_av; 
    results* r_b = (results *)r_bv; 
    results* r_c = (results *)r_cv; 
    results* r_d = (results *)r_dv; 
    /*Print statistics*/
    append_to_file(fname, 1, r_a->infected + r_b->infected + r_c->infected + r_d->infected, r_a->immune + r_b->immune + r_c->immune + r_d->immune, r_a->healthy + r_b->healthy + r_c-> healthy + r_d->healthy, count_turtles);
    //printf("llega aqui\n");
    
    /*Repeat the process for the number of week set by the user, updating new values for statistics and matrix of patches*/
    for(int i = 0; i < (sim_weeks - 1); i++){
        //printf("MATRIZ EN LA SEMANA: %d\n", i);
        //print_mat(mat);
        //printf("primer for\n");
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied && !mat[x][y].moved){
                    //printf("DENTRO DE IF OCCUPIED\n");
                    get_older(x, y);
                    int pos[2]; // pos[0] = moved x, pos[1] = moved y
                    if(!died){
                        move(pos, mat[x][y].humano, x, y);
                    }
                }
                 
            }    
        }
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied){
                    if(mat[x][y].humano->sick){
                        recover_or_die(duration, chance_recover, x, y);
                        if(!died){
                            infect(mat[x][y].humano, infectiousness, x, y);
                        }
                    }
                    else{
                        reproduce(number_people, chance_reproduce);
                    }
                    mat[x][y].moved = 0;
                    
                }
                    
            }    
        }
        a.number_people = count_turtles;
        b.number_people = count_turtles;
        c.number_people = count_turtles;
        d.number_people = count_turtles;
        pthread_create(&t1,NULL,update_global_variables,&a);
        pthread_create(&t2,NULL,update_global_variables,&b);
        pthread_create(&t3,NULL,update_global_variables,&c);
        pthread_create(&t4,NULL,update_global_variables,&d);   
        pthread_join(t1, &r_av);
        pthread_join(t2, &r_bv);
        pthread_join(t3, &r_cv);
        pthread_join(t4, &r_dv);
        r_a = (results *)r_av; 
        r_b = (results *)r_bv; 
        r_c = (results *)r_cv; 
        r_d = (results *)r_dv; 
        append_to_file(fname, i+2, r_a->infected + r_b->infected + r_c->infected + r_d->infected, r_a->immune + r_b->immune + r_c->immune + r_d->immune, r_a->healthy + r_b->healthy + r_c-> healthy + r_d->healthy, count_turtles);
        //printf("ULTIMO PRINT\n");
        //print_mat(mat);
        //printf("\n\n");
        
        
    }
}






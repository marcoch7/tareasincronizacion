#include "write_file.h"
#include "stdbool.h"

#define DIM 100
/* 
 * STRUCTS
 */

/* Human data */
typedef struct
{
    /* Indicates if the human is sick */
    int sick;

    /* Indicates human's remaining_immunity */
    int remaining_inmunity;

    /* Indicates human's sick_time */
    int sick_time;

    /* Indicates human's age */
    int age;
}human;//

/* Patch matrix */
typedef struct
{
    
    human* humano;

    /* Indicates a patch is occupied */
    bool occupied;

    /* Indicates a patch's member was moved this week */
    bool moved;
}patch;

/* Results struct */
typedef struct 
{   
    /* Infected % */
    float infected;

    /* Immune % */
    float immune;

    /* Healthy % */
    float healthy;
}results;

/* Limits struct for threads*/
typedef struct 
{
    /* lower limit on x axis for a thread*/
    int x_inf;

    /* lower limit on y axis for a thread*/
    int y_inf;

    /* upper limit on x axis for a thread*/
    int x_sup;

    /* upper limit on y axis for a thread*/
    int y_sup;

    /* total of current people*/
    int number_people;
}limits;


/* 
 *  Functions
 */

/*
 * Fill a default matrix of patch
 */
void fill_mat();

/*
 * Initialize the matrix with the first humans for the model.
 * 
 * [in] turtles :   Number of humans to star the model.
 */
void matriz_human(int turtles);

/*
 * Print the current state of the matrix.
 */
void print_mat();


/*
 * Makes a human sick.
 * 
 * [in] humanP : Pointer to human.
 */
void get_sick(human* humanP);

/*
 * Makes a human healthy.
 * 
 * [in] humanP : Pointer to human.
 */
void get_healthy(human* humanP);

/*
 * Makes a human immune.
 * 
 * [in] humanP : Pointer to human.
 */
void become_immune(human* humanP);

/*
 * Makes a human older and kills a human if their age is greater than lifespan.
 * 
 * [in] x : x coordinate of human in the matrix.
 * [in] y : y coordinate of human in the matrix.
 */
void get_older(int x , int y);

/*
 * Checks whether a human recovered or died.
 * 
 * [in] duration : duration of the virus in a human.
 * [in] chance_recover : chance of recovering from the virus.
 * [in] x : x coordinate of human in the matrix.
 * [in] y : y coordinate of human in the matrix.
 */
void recover_or_die(int duration, int chance_recover, int x, int y);

/*
 * Main function of the simulation, use the input from the user to set the parameters for simulation.
 * It handles the changes in the simulation over the weeks that it lasts.
 * 
 * [in] number_people : Initial number of Humans.
 * [in] infectiousness: How infectious the virus is.
 * [in] chance_recover : chance of recovering from the virus.
 * [in] duration : duration of the virus in a human.
 * [in] sim_weeks : Number of weeks to be simulated.
 */
void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks);

/*
 * Thread process to update global variables in the simulation
 * 
 * [in] arg : Pointer to a struct with the arguments of the function.
 * [out] : Pointer with the return values of the function.
 */
void* update_global_variables(void* arg);

/*
 * Moves a human to a new position.
 * 
 * [in] humanP : Pointer to human.
 * [in] i : current x coordinate of human in the matrix.
 * [in] j : current y coordinate of human in the matrix.
 */
void move(int pos[], human* humanP, int i, int j);

/*
 * Infects a human with the virus.
 * 
 * [in] humanP : Pointer to human.
 * [in] infectiousness: How infectious the virus is.
 * [in] i : current x coordinate of human in the matrix.
 * [in] j : current y coordinate of human in the matrix.
 */
void infect(human* humanP, int infectiousness, int i, int j);

/*
 * Checks whether a human will reproduce or not.
 * 
 * [in] number_people : current number of humans in the matrix.
 * [in] chance_reproduce : chance for a human to be reproduced.
 */
void reproduce(int number_people, int chance_reproduce);

/*
 * Creates a new human
 */
void hatch();

/*
 * Sets up  the initial humans in the matrix.
 * 
 * [in] number_people : Number of humans to start the model.
 * [in] HumanP : Pointer to a human.
 */
void setup_turtles(human* humanP, int number_people);

/*
 * Sets up the global variables that are constant.
 */
void setup_constants();




#include "write_file.h"
#include <stdio.h>
#include <string.h>

void write_file(char* fname){
    FILE* fpointer = fopen(fname, "w");
    fprintf(fpointer, "Week, Sick, Sick percentage, Immune, Immune percentage, Healthy, Healthy percentage, Total\n");
    fclose(fpointer);
}

void append_to_file(char* fname, int week, float infected, float immune, float healthy, int p_total){
    
    FILE* fpointer = fopen(fname, "a");
    
    fprintf(fpointer, "%d, %d, %f, %d, %f, %d, %f, %d\n", week, (int)((p_total*infected)/100), infected, (int)((p_total*immune)/100), immune, (int)((p_total*healthy)/100), healthy, p_total);
   
    fclose(fpointer);
}
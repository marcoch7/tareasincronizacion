#include "part1.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>


int lifespan = 0;             //the lifespan of a turtle
int chance_reproduce = 0;     //the probability of a turtle generating an offspring each tick
int carrying_capacity = 0;   //the number of turtles that can be in the world at one time
int immunity_duration = 0;
int sick_q = 0;
int count_turtles = 0;
int week = 0;
int died = 0;
float p_infected = 0;
float p_immune = 0;
float p_healthy = 0;
int p_total = 0;
int rand_move = 0;

patch mat[DIM][DIM];

void fill_mat(){
    /*Fill th matrix with NULL pointer to human and a not occupied patch*/
    for(int i = 0; i < DIM; i++)
    {
        for (int j = 0; j < DIM; j++)
        {
            mat[i][j].humano = NULL;
            mat[i][j].occupied = false;
        }
            
    }
}


human* human_data(){
    /*Allocate space for a human in memory*/
    human* humanP = malloc(sizeof(human));
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;
    humanP->age = 0;
    return humanP;
}



void matriz_human(int number_people){
    /*Generate random numbers with time as seed*/
    srand(time(NULL));
    for(int i = 0; i < number_people; i++){
        /*Generate random x,y coordinate*/
        int x = rand()%DIM;
        int y = rand()%DIM;
        /*Coordinates must not go to an occupied field*/
        while (mat[x][y].occupied == 1)
        {
            x = rand()%DIM;
            y = rand()%DIM;
        }
        mat[x][y].humano = human_data();
        mat[x][y].occupied = true;
        mat[x][y].moved = false;
        setup_turtles(mat[x][y].humano, number_people);
    }
}

void print_mat(){
    /*Print the matriz showing tha age of each human*/
    for(int i = 0; i < DIM; i++){
        for(int j = 0; j < DIM; j++){
            if(mat[i][j].humano != NULL){
                printf("%p, %d |", mat[i][j].humano, mat[i][j].humano->age);
            }
            else
            {
                printf("%d |", 0);
            }
        }
        printf("\n");
    }
}

results* results_data(){
    /*Allocate space for results in memory*/
    results* resultsP = malloc(sizeof(results));
    resultsP->infected = 0;
    resultsP->immune = 0;
    resultsP->healthy = 0;
    return resultsP;
}

void setup_constants(){
    srand(time(NULL));
    lifespan = 2600;
    carrying_capacity = 300;
    chance_reproduce = 1;
    immunity_duration = 52;
}

void get_sick(human* humanP){
    humanP->sick = 1;
    humanP->remaining_inmunity = 0;
}

void get_healthy(human* humanP){
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;  
}

void become_immune(human* humanP){
    humanP->sick = 0;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = immunity_duration;
}

void get_older(int x, int y){
    died = 0;
    /*Age increases by 1*/
    mat[x][y].humano->age++;
    /*Check if human is old enough to die*/
    if(mat[x][y].humano->age > lifespan){
        died = 1;
        mat[x][y].humano->age = 0;
        mat[x][y].humano->remaining_inmunity = 0;
        mat[x][y].humano->sick = 0;
        mat[x][y].humano->sick_time = 0;
        mat[x][y].moved = 0;
        mat[x][y].occupied = 0;
        count_turtles--;
    } 
    /*If human did not die*/
    if(!died && mat[x][y].humano != NULL){
        /*If still immune,, reduce immunity*/
        if(mat[x][y].humano->remaining_inmunity > 0){
            mat[x][y].humano->remaining_inmunity--;
        }
        /*If sick increase sick time*/
        if(mat[x][y].humano->sick){
            mat[x][y].humano->sick_time++;
        }
    }   
}

void recover_or_die(int duration, int chance_recover, int x, int y){
    if((mat[x][y].humano != NULL)){
        /*If human has been sick for longer than the duration of the virus*/
        if(mat[x][y].humano->sick_time > duration){
            /*Human will become immune*/
            if((rand() % 100) < chance_recover){
                become_immune(mat[x][y].humano);
            }
            /*Or human will die*/
            else{
                mat[x][y].humano->age = 0;
                mat[x][y].humano->remaining_inmunity = 0;
                mat[x][y].humano->sick = 0;
                mat[x][y].humano->sick_time = 0;
                mat[x][y].moved = 0;
                mat[x][y].occupied = 0;
                died = 1;
                count_turtles--;
            }
        }
    }    
}


void update_global_variables(int number_people){
    float c_infected = 0;
    float c_immune = 0;
    float c_healthy = 0;
    int occu = 0;
    /*If there is still people alive*/
    if(number_people > 0){
        /*Check for occupied patches and update global variables*/
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if(mat[i][j].occupied){  
                    occu++;
                
                    if((mat[i][j].humano->sick)){
                        c_infected++;
                    }
                    if((mat[i][j].humano->remaining_inmunity > 0)){
                        c_immune++;
                    }
                    if((mat[i][j].humano->sick == 0) && (mat[i][j].humano->remaining_inmunity == 0)){
                        c_healthy++;
                    }
                    
                }
            }       
        }      
    }
    p_infected = (c_infected/number_people)*100;
    p_immune = (c_immune/number_people)*100;
    p_healthy = (c_healthy/number_people)*100;
    p_total = occu;
}

void move(int pos[], human* humanP, int i, int j){
    /*Move with case 1-3, or set x = 1*/
    rand_move++;
    if(rand_move == 4){
        rand_move = 0;
    }
    int x = 0;
    int y = 0;
    if((rand_move == 0) && (i+1 < DIM) && (j+1 < DIM)){
        x = i+1; 
        y = j+1;
        
    }
    else if((rand_move == 1) && (i+1 < DIM) && (j-1 > 0)){
        x = i+1; 
        y = j-1;
        
        
    }
    else if((rand_move == 2) && (i-1 > 0) && (j+1 < DIM)){
        x = i-1; 
        y = j+1;
        
    }
    else if((rand_move == 3) && (i-1 > 0) && (j-1 > 0)){
        x = i-1; 
        y = j-1;
        
    }
    else{
        x = 1;
        
    }
    /*Keep moving if patch is occupied (2D matrix)*/
    while (mat[x][y].occupied == 1)
    {
        if(x+1 < DIM){
            x++;
        }
        else{
            x = 0;
        }
        if(mat[x][y].occupied == 1){
            if(y+1 < DIM){
                y++;
            }
            else{
                y = 0;
            }
        }

    }
    /*Update human position*/
    mat[x][y].humano = human_data();
    mat[x][y].humano->age = mat[i][j].humano->age;
    mat[x][y].humano->sick = mat[i][j].humano->sick;
    mat[x][y].humano->sick_time = mat[i][j].humano->sick_time;
    mat[x][y].humano->remaining_inmunity = mat[i][j].humano->remaining_inmunity;

    mat[x][y].occupied = true;
    mat[x][y].moved = true;
    mat[i][j].humano = NULL;
    mat[i][j].occupied = 0;
    mat[i][j].moved = 0;
    pos[0] = x;
    pos[1] = y;
}

void infect(human* humanP, int infectiousness, int i, int j){
    for(int x = 0; x < DIM; x++){
        for(int y = 0; y < DIM; y++){
            /* Occupied, Not sick, not immune and adyacent patches*/
            if((mat[x][y].occupied) && !(mat[x][y].humano->sick) && (mat[x][y].humano->remaining_inmunity == 0) && ((x == i+1 && (y == j+1 || y == j-1)) || (x == i-1 && (y == j+1 || y == j-1)))){
                int rand_infect = rand() % 100;
                if(rand_infect < infectiousness){
                    get_sick(mat[x][y].humano);
                }
            }
        }
    }
}

void hatch(){
    /*Generate random x,y coordinate*/
    int x = rand()%DIM;
    int y = rand()%DIM;
    /*Coordinates must not go to and occupied field*/
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    mat[x][y].humano = human_data();
    mat[x][y].occupied = true;
    mat[x][y].moved = false;
    get_healthy(mat[x][y].humano);
    mat[x][y].humano->age = 1;
    count_turtles++;
}

void reproduce(int number_people, int chance_reproduce){
    int rand_reproduce = rand() % 100;
    /*If there is still room for more humans and the chance of reproduce is correct, create a new human*/
    if((count_turtles < carrying_capacity) && (rand_reproduce < chance_reproduce)){
        hatch(mat);
    }
}

void setup_turtles(human* humanP, int number_people){
    int sick = rand() % number_people;
    humanP->age = rand() % lifespan;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = 0;
    get_healthy(humanP);
    /*Probability of being sick for the first humans in the model*/
    if(sick < (number_people/4 + 1)){
        get_sick(humanP);
    }
    else{
        //printf("NOT SICK\n");
    }
}

void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks){
    /*Set initial conditions*/
    setup_constants();
    fill_mat();
    sick_q = 0;
    char* fname = malloc(25*sizeof(char));
    fname = "part1.csv";
    write_file(fname);
    count_turtles = number_people;
    /*Create matrix with number_people, first week*/
    matriz_human(number_people);
    update_global_variables(count_turtles);
    append_to_file(fname, 1, p_infected, p_immune, p_healthy, p_total);
    /*weeks 2 to sim_weeks-1*/
    for(int i = 0; i < (sim_weeks - 1); i++){
        for(int x = 0; x < DIM; x++){
            for(int y = 0; y < DIM; y++){
                /*Get each human older each week*/
                if(mat[x][y].occupied && !mat[x][y].moved){
                    get_older(x, y);
                    int pos[2];
                    /*If human didn't die, move them*/
                    if(!died){
                        move(pos, mat[x][y].humano, x, y);
                    }
                } 
            }    
        }
        /*After they've been moved/died, either recover_or_die or reproduce*/
        for(int x = 0; x < DIM; x++){
            for(int y = 0; y < DIM; y++){
                if(mat[x][y].occupied){
                    if(mat[x][y].humano->sick){
                        recover_or_die(duration, chance_recover, x, y);
                        if(!died){
                            infect(mat[x][y].humano, infectiousness, x, y);
                        }
                    }
                    else{
                        reproduce(number_people, chance_reproduce);
                    }
                    mat[x][y].moved = 0;
                    
                }
                    
            }    
        }
        update_global_variables(count_turtles);
        append_to_file(fname, i+2, p_infected, p_immune, p_healthy, p_total);    
    }

}






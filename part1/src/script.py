import numpy as np
import csv
from matplotlib import pyplot as plt

value = input("Enter weeks : ") 
weeks = int(value)
weeks_list = list(range(0, (weeks)))

sick = [0]*weeks
immune = [0]*weeks
healthy = [0]*weeks
total = [0]*weeks

with open('part1.csv') as csvfile:
    creader = csv.reader(csvfile, delimiter =',')
    for row in creader:
        for i in range(weeks):
            if row[0] == str(i):
                sick[i] += float(row[1])
                immune[i] += float(row[3])
                healthy[i] += float(row[5])
                total[i] += float(row[7])


plt.plot(weeks_list, sick, color='red')
plt.grid(linestyle='-', linewidth=0.5)
plt.title("Sick people vs weeks")
plt.ylabel("Sick people")
plt.xlabel("Weeks")
plt.savefig('images/part1_sick.png')
plt.close()

plt.plot(weeks_list, immune, color='orange')
plt.grid(linestyle='-', linewidth=0.5)
plt.title("Immune people vs weeks")
plt.ylabel("Immune people")
plt.xlabel("Weeks")
plt.savefig('images/part1_immune.png')
plt.close()

plt.plot(weeks_list, healthy, color='yellow')
plt.grid(linestyle='-', linewidth=0.5)
plt.title("Healthy people vs weeks")
plt.ylabel("Healthy people")
plt.xlabel("Weeks")
plt.savefig('images/part1_healthy.png')
plt.close()

plt.plot(weeks_list, total, color='blue', label ='total')
plt.plot(weeks_list, sick, color='red', label ='sick')
plt.plot(weeks_list, immune, color='orange', label ='immune')
plt.plot(weeks_list, healthy, color='yellow', label ='healthy')
plt.grid(linestyle='-', linewidth=0.5)
plt.title("Sick, immune and healthy people vs weeks")
plt.ylabel("People")
plt.xlabel("Weeks")
plt.legend()
plt.savefig('images/part1_all.png')
plt.close()

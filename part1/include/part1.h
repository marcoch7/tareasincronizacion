#include "write_file.h"
#include "stdbool.h"

#define DIM 100
/* 
 * STRUCTS
 */

/* Human data */
typedef struct
{
    /* Indicates if the human is sick */
    int sick;

    /* Indicates human's remaining_immunity */
    int remaining_inmunity;

    /* Indicates human's sick_time */
    int sick_time;

    /* Indicates human's age */
    int age;
}human;//

/* Patch matrix */
typedef struct
{
    
    human* humano;
    /* Indicates a patch is occupied */
    bool occupied;

    /* Indicates a patch's member was moved this week */
    bool moved;
}patch;

/* Results struct */
typedef struct 
{
    /* Infected % */
    float infected;

    /* Immune % */
    float immune;

    /* Healthy % */
    float healthy;
}results;

void fill_mat();

void matriz_human(int turtles);

void print_mat();

void get_sick(human* humanP);

void get_healthy(human* humanP);

void become_immune(human* humanP);

void get_older(int x , int y);

void recover_or_die(int duration, int chance_recover, int x, int y);

void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks);

void update_global_variables(int number_people);

void move(int pos[], human* humanP, int i, int j);

void infect(human* humanP, int infectiousness, int i, int j);

void reproduce(int number_people, int chance_reproduce);

void hatch();

void setup_turtles(human* humanP, int number_people);

void setup_constants();




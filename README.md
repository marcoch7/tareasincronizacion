# Tarea de Sincronizacion

## Compile

In order to compile, type in console:

```c
make compile
```

## Exe

In order to create the executable file, type in console:

```c
make exe
```
## Run

In order to run the simulation, type in console:

```c
make run
```

## Program description

This program runs a simulation of the impact of a virus in a group of people, showing how many people get infected, is still healthy and become immune over the weeks set by the user in a plot.

## Additional information

### Parameters

There are 5 parameters that can be set by the user when running the simlation:

1. Initial number of people
2. Infectiousness
3. Chance of recovery
4. Duration of the virus in weeks
5. Weeks for the simulation

Additionally the program also receives how many week are goint to be plotted.

### Commentaries

-There are two folders with two separate programs, one that runs without threads and one that use threads to improve the performance.

-The '.h' files are located in the 'include' folder, these files have a detailed description for each function and structure.

-The '.c' files are located in the 'src' folder, these files have a brief description of the logic for each function.


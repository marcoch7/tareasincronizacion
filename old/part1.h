#include "write_file.h"
#include "stdbool.h"

#define DIM 10

typedef struct
{
    int sick;
    int remaining_inmunity;
    int sick_time;
    int age;
}human;//

typedef struct
{
    human* humano;
    bool occupied;
    bool moved;
}patch;

typedef struct 
{
    float infected;
    float immune;
    float healthy;
}results;

void matriz_human(int turtles, patch mat[DIM][DIM]);

void print_mat(patch mat[DIM][DIM]);

void get_sick(human* humanP);

void get_healthy(human* humanP);

void become_immune(human* humanP);

void get_older(patch mat[DIM][DIM], int x , int y);

void recover_or_die(patch mat[DIM][DIM], int duration, int chance_recover, int x, int y);

void setup(int number_people, int infectiousness, int chance_recover, int duration, patch mat[DIM][DIM], int sim_weeks);

void update_global_variables(results* result, patch mat[DIM][DIM], int number_people);

void move(int pos[], human* humanP, patch mat[DIM][DIM], int i, int j);

void infect(human* humanP, patch mat[DIM][DIM], int infectiousness, int i, int j);

void reproduce(int number_people, int chance_reproduce, patch mat[DIM][DIM]);

void hatch(patch mat[DIM][DIM]);

void setup_turtles(human* humanP, int number_people);

void setup_constants();




#include "part1.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

// CONSTANTS. Maybe add them to a struct? 
int lifespan = 0;             //the lifespan of a turtle
int chance_reproduce = 0;     //the probability of a turtle generating an offspring each tick
int carrying_capacity = 0;   //the number of turtles that can be in the world at one time
int immunity_duration = 0;
int sick_q = 0;
int count_turtles = 0;
int week = 0;
int died = 0;

human* human_data(){
    human* humanP = malloc(sizeof(human));
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;
    humanP->age = 0;
    return humanP;
}



void matriz_human(int number_people, patch mat[DIM][DIM]){
    srand(time(NULL));
    for(int i = 0; i < number_people; i++){
        int x = rand()%DIM;
        int y = rand()%DIM;
        while (mat[x][y].occupied == 1)
        {
            x = rand()%DIM;
            y = rand()%DIM;
        }
        mat[x][y].humano = human_data();
        mat[x][y].occupied = true;
        mat[x][y].moved = false;
        setup_turtles(mat[x][y].humano, number_people);
    }
}

void print_mat(patch mat[DIM][DIM]){
    //printf("ENTRA A PRINTT\n");
    for(int i = 0; i < DIM; i++){
        for(int j = 0; j < DIM; j++){
            if(mat[i][j].humano != NULL){
                printf("%d |", mat[i][j].humano->age);
            }
            else
            {
                printf("%d |", 0);
            }
        }
        printf("\n");
    }
}

results* results_data(){
    results* resultsP = malloc(sizeof(results));
    resultsP->infected = 0;
    resultsP->immune = 0;
    resultsP->healthy = 0;
    return resultsP;
}

void setup_constants(){
    srand(time(NULL));
    lifespan = 2600; //* (rand() % 40);
    carrying_capacity = 300;
    chance_reproduce = 1;
    immunity_duration = 52;
}

void get_sick(human* humanP){
    humanP->sick = 1;
    humanP->remaining_inmunity = 0;
    //printf("SE ENFERMO %d\n", humanP->age);
}

void get_healthy(human* humanP){
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;  
}

void become_immune(human* humanP){
    humanP->sick = 0;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = immunity_duration;
}

void get_older(patch mat[DIM][DIM], int x, int y){
    printf("Entra a get older\n");
    died = 0;
    mat[x][y].humano->age++;
    if(mat[x][y].humano->age > lifespan){
        printf("MUERE\n");
        //free(humanP);
        died = 1;
        mat[x][y].humano->age = 0;
        mat[x][y].humano->remaining_inmunity = 0;
        mat[x][y].humano->sick = 0;
        mat[x][y].humano->sick_time = 0;
        mat[x][y].moved = 0;
        mat[x][y].occupied = 0;
        //mat[x][y].humano = NULL;
        //printf("se libera una\n");
    } 
    //printf("Sale con la edad: %d\n", humanP->age);
    if(!died && mat[x][y].humano != NULL){
        printf("NO MUERE\n");
        if(mat[x][y].humano->remaining_inmunity > 0){
        mat[x][y].humano->remaining_inmunity--;
        }
        if(mat[x][y].humano->sick){
            mat[x][y].humano->sick_time++;
        }
    }
    
}

void recover_or_die(patch mat[DIM][DIM], int duration, int chance_recover, int x, int y){
    printf("en recover or die, sicktime: %d, duration: %d\n", mat[x][y].humano->sick_time, duration);
    if(mat[x][y].humano->sick_time > duration){
        printf("the turtle has survived past the virus duration");
        if((rand() % 100) < chance_recover){
            become_immune(mat[x][y].humano);
            printf("un become_immune\n");
        }
        else{
            //free(humanP);
            mat[x][y].humano->age = 0;
            mat[x][y].humano->remaining_inmunity = 0;
            mat[x][y].humano->sick = 0;
            mat[x][y].humano->sick_time = 0;
            mat[x][y].moved = 0;
            mat[x][y].occupied = 0;
            mat[x][y].humano = NULL;
            died = 1;
            count_turtles--;
            printf("un died\n");
        }
    }
}


void update_global_variables(results* result, patch mat[DIM][DIM], int number_people){
    float c_infected = 0;
    float c_immune = 0;
    float c_healthy = 0;
    if(number_people > 0){
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if(mat[i][j].occupied){  
                
                    if((mat[i][j].humano->sick) && (mat[i][j].humano->remaining_inmunity == 0)){
                        c_infected++;
                    }
                    if((mat[i][j].humano->remaining_inmunity > 0)){
                        c_immune++;
                    }
                    if((mat[i][j].humano->sick == 0) && (mat[i][j].humano->remaining_inmunity == 0)){
                        c_healthy++;
                    }
                    
                }
            }       
        }      
    }
    result->infected = (c_infected/number_people)*100;
    result->immune = (c_immune/number_people)*100;
    result->healthy = (c_healthy/number_people)*100;
    printf("hizo update, infected: %f, immune: %f\n", result->infected, result->immune);
}

void move(int pos[], human* humanP, patch mat[DIM][DIM], int i, int j){
    //printf("HACE MOVE\n");
    srand(time(NULL));
    int x = rand() % DIM;
    int y = rand() % DIM;
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    mat[x][y].humano = mat[i][j].humano;
    mat[x][y].occupied = true;
    mat[x][y].moved = true;
    mat[i][j].occupied = 0;
    mat[i][j].moved = 0;
    //free(humanP);
    mat[i][j].humano = NULL;
    pos[0] = x;
    pos[1] = y;
}

void infect(human* humanP, patch mat[DIM][DIM], int infectiousness, int i, int j){
    for(int x = 0; x < DIM; x++){
        for(int y = 0; y < DIM; y++){
            /* Occupied, Not sick, not immune and adyacent patches*/
            if((mat[x][y].occupied) && !(mat[x][y].humano->sick) && (mat[x][y].humano->remaining_inmunity == 0) && ((x == i+1 && (y == j+1 || y == j-1)) || (x == i-1 && (y == j+1 || y == j-1)))){
                int rand_infect = rand() % 100;
                if(rand_infect < infectiousness){
                    //printf("UN GET SICK DESDE INFECT, i: %d, j: %d, x: %d, y: %d\n", i, j, x, y);
                    get_sick(mat[x][y].humano);
                }
            }
        }
    }
}

void hatch(patch mat[DIM][DIM]){
    //printf("UN HATCH\n");
    int x = rand()%DIM;
    int y = rand()%DIM;
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    mat[x][y].humano = human_data();
    mat[x][y].occupied = true;
    mat[x][y].moved = false;
    get_healthy(mat[x][y].humano);
    mat[x][y].humano->age = 1;
    count_turtles++;
}

void reproduce(int number_people, int chance_reproduce, patch mat[DIM][DIM]){
    int rand_reproduce = rand() % 100;
    if((count_turtles < carrying_capacity) && (rand_reproduce < chance_reproduce)){
        hatch(mat);
    }
}

void setup_turtles(human* humanP, int number_people){
    int sick = rand() % number_people;
    humanP->age = rand() % lifespan;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = 0;
    get_healthy(humanP);
    printf("TORTUGA: %d\n", humanP->age);
    if(sick < (number_people/4 + 1)){
        printf("Se va a enfermar\n");
        get_sick(humanP);
    }
    else{
        printf("NOT SICK\n");
    }
}

void setup(int number_people, int infectiousness, int chance_recover, int duration, patch mat[DIM][DIM], int sim_weeks){
    setup_constants();
    results* resultsP = results_data();
    //printf("lifespan: %d\n", lifespan);
    sick_q = 0;
    char* fname = malloc(25*sizeof(char));
    fname = "part1.csv";
    write_file(fname);
    count_turtles = number_people;
    matriz_human(number_people, mat);
    //printf("sale de matriz\n");
    update_global_variables(resultsP, mat, count_turtles);
    append_to_file(fname, 1, resultsP->infected, resultsP->immune, resultsP->healthy);
    //printf("llega aqui\n");
    
    for(int i = 0; i < (sim_weeks - 1); i++){
        printf("MATRIZ EN LA SEMANA: %d\n", i);
        //print_mat(mat);
        //printf("primer for\n");
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied && !mat[x][y].moved){
                    //printf("DENTRO DE IF OCCUPIED\n");
                    get_older(mat, x, y);
                    int pos[2]; // pos[0] = moved x, pos[1] = moved y
                    move(pos, mat[x][y].humano, mat, x, y);
                    /*if(mat[pos[0]][pos[1]].humano->sick){
                        printf("UN SICK TO RECOVER & INFECT\n");
                        recover_or_die(mat, duration, chance_recover, pos[0], pos[1]);
                        if(!died){
                            infect(mat[pos[0]][pos[1]].humano, mat, infectiousness, pos[0], pos[1]);
                        }
                        
                    }
                    else{
                        //printf("UN REPRODUCE\n");
                        reproduce(number_people, chance_reproduce, mat);
                    } */  
                }
                 
            }    
        }
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied && mat[x][y].moved){
                    if(mat[x][y].humano->sick){
                        recover_or_die(mat, duration, chance_recover, x, y);
                        if(!died){
                            infect(mat[x][y].humano, mat, infectiousness, x, y);
                        }
                    }
                    else{
                        reproduce(number_people, chance_reproduce, mat);
                    }
                    
                }
                    
            }    
        }
        update_global_variables(resultsP, mat, count_turtles);
        append_to_file(fname, i+2, resultsP->infected, resultsP->immune, resultsP->healthy);
        printf("ULTIMO PRINT\n");
        //print_mat(mat);
        for(int q = 0; q < DIM; q++){
            //printf("segundo for\n");
            for(int r = 0; r < DIM; r++){
                if(mat[q][r].occupied == 1){
                    mat[q][r].moved = 0;
                    
                }
            }
        }        
        printf("\n\n");
        
        
    }

    //append_to_file(fname, week, humanP->sick, resultsP->infected, resultsP->immune);
}






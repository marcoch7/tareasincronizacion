#include "write_file.h"
#include "stdbool.h"

#define DIM 100

typedef struct
{
    int sick;
    int remaining_inmunity;
    int sick_time;
    int age;
}human;//

typedef struct
{
    human* humano;
    bool occupied;
    bool moved;
}patch;

typedef struct 
{
    float infected;
    float immune;
    float healthy;
}results;

typedef struct 
{
    int x_inf;
    int y_inf;
    int x_sup;
    int y_sup;
    int number_people;
}limits;

void fill_mat();

void matriz_human(int turtles);

void print_mat();

void get_sick(human* humanP);

void get_healthy(human* humanP);

void become_immune(human* humanP);

void get_older(int x , int y);

void recover_or_die(int duration, int chance_recover, int x, int y);

void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks);

void* update_global_variables(void* arg);

void move(int pos[], human* humanP, int i, int j);

void infect(human* humanP, int infectiousness, int i, int j);

void reproduce(int number_people, int chance_reproduce);

void hatch();

void setup_turtles(human* humanP, int number_people);

void setup_constants();




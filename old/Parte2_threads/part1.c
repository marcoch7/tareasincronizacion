#include "part1.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include<pthread.h>


int lifespan = 0;             //the lifespan of a turtle
int chance_reproduce = 0;     //the probability of a turtle generating an offspring each tick
int carrying_capacity = 0;   //the number of turtles that can be in the world at one time
int immunity_duration = 0;
int sick_q = 0;
int count_turtles = 0;
int week = 0;
int died = 0;
float p_infected = 0;
float p_immune = 0;
float p_healthy = 0;

patch mat[DIM][DIM];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void fill_mat(){
    for(int i = 0; i < DIM; i++)
    {
        for (int j = 0; j < DIM; j++)
        {
            mat[i][j].humano = NULL;
            mat[i][j].occupied = false;
        }
            
    }
}


human* human_data(){
    human* humanP = malloc(sizeof(human));
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;
    humanP->age = 0;
    return humanP;
}



void matriz_human(int number_people){
    srand(time(NULL));
    for(int i = 0; i < number_people; i++){
        int x = rand()%DIM;
        int y = rand()%DIM;
        while (mat[x][y].occupied == 1)
        {
            x = rand()%DIM;
            y = rand()%DIM;
        }
        mat[x][y].humano = human_data();
        mat[x][y].occupied = true;
        mat[x][y].moved = false;
        setup_turtles(mat[x][y].humano, number_people);
    }
}

void print_mat(){
    //printf("ENTRA A PRINTT\n");
    for(int i = 0; i < DIM; i++){
        for(int j = 0; j < DIM; j++){
            if(mat[i][j].humano != NULL){
                printf("%p, %d |", mat[i][j].humano, mat[i][j].humano->age);
            }
            else
            {
                printf("%d |", 0);
            }
        }
        printf("\n");
    }
}

results* results_data(){
    results* resultsP = malloc(sizeof(results));
    resultsP->infected = 0;
    resultsP->immune = 0;
    resultsP->healthy = 0;
    return resultsP;
}

void setup_constants(){
    srand(time(NULL));
    lifespan = 2600; //* (rand() % 40);
    carrying_capacity = 300;
    chance_reproduce = 1;
    immunity_duration = 52;
}

void get_sick(human* humanP){
    humanP->sick = 1;
    humanP->remaining_inmunity = 0;
    //printf("SE ENFERMO %d\n", humanP->age);
}

void get_healthy(human* humanP){
    humanP->sick = 0;
    humanP->remaining_inmunity = 0;
    humanP->sick_time = 0;  
}

void become_immune(human* humanP){
    humanP->sick = 0;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = immunity_duration;
}

void get_older(int x, int y){
    printf("Entra a get older age: %d\n", mat[x][y].humano->age);
    died = 0;
    mat[x][y].humano->age++;
    if(mat[x][y].humano->age > lifespan){
        printf("MUERE\n");
        //free(humanP);
        died = 1;
        mat[x][y].humano->age = 0;
        mat[x][y].humano->remaining_inmunity = 0;
        mat[x][y].humano->sick = 0;
        mat[x][y].humano->sick_time = 0;
        mat[x][y].moved = 0;
        mat[x][y].occupied = 0;
        count_turtles--;
        //mat[x][y].humano = NULL;
        //printf("se libera una\n");
    } 
    //printf("Sale con la edad: %d\n", humanP->age);
    if(!died && mat[x][y].humano != NULL){
        printf("NO MUERE\n");
        if(mat[x][y].humano->remaining_inmunity > 0){
            mat[x][y].humano->remaining_inmunity--;
        }
        if(mat[x][y].humano->sick){
            mat[x][y].humano->sick_time++;
        }
    }
    printf("Sale de get older age: %d\n", mat[x][y].humano->age);
    
}

void recover_or_die(int duration, int chance_recover, int x, int y){
    if((mat[x][y].humano != NULL)){
        printf("en recover or die, sicktime: %d, duration: %d\n", mat[x][y].humano->sick_time, duration);
        if(mat[x][y].humano->sick_time > duration){
            printf("the turtle has survived past the virus duration");
            if((rand() % 100) < chance_recover){
                become_immune(mat[x][y].humano);
                printf("un become_immune\n");
            }
            else{
                //free(humanP);
                mat[x][y].humano->age = 0;
                mat[x][y].humano->remaining_inmunity = 0;
                mat[x][y].humano->sick = 0;
                mat[x][y].humano->sick_time = 0;
                mat[x][y].moved = 0;
                mat[x][y].occupied = 0;
                //mat[x][y].humano = NULL;
                died = 1;
                count_turtles--;
                printf("un died\n");
            }
        }
    }    
}


void* update_global_variables(void *arg){
    results* results = malloc(sizeof(results));
    limits * lim = (limits *)(arg);
    float c_infected = 0;
    float c_immune = 0;
    float c_healthy = 0;
    int occu = 0;
    if(lim->number_people > 0){
        for (int i = lim->x_inf; i < lim->x_sup; i++)
        {
            for (int j = lim->y_inf; j < lim->y_sup; j++)
            {
                if(mat[i][j].occupied){  
                    occu++;
                
                    if((mat[i][j].humano->sick)){
                        c_infected++;
                    }
                    if((mat[i][j].humano->remaining_inmunity > 0)){
                        c_immune++;
                    }
                    if((mat[i][j].humano->sick == 0) && (mat[i][j].humano->remaining_inmunity == 0)){
                        c_healthy++;
                    }
                    
                }
            }       
        }      
    }
    pthread_mutex_lock(&mutex);
    p_infected = (c_infected/lim->number_people)*100;
    p_immune = (c_immune/lim->number_people)*100;
    p_healthy = (c_healthy/lim->number_people)*100;
    pthread_mutex_unlock(&mutex);
    results->infected = p_infected;
    results->immune = p_immune;
    results->healthy = p_healthy;
    printf("hizo update, infected: %f, immune: %f, healthy: %f, occu: %d, count tortugas: %d\n", p_infected, p_immune, p_healthy, occu, count_turtles);
    pthread_exit(results);
}

void move(int pos[], human* humanP, int i, int j){
    //printf("HACE MOVE\n");
    srand(time(NULL));
    int x = rand() % DIM;
    int y = rand() % DIM;
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    printf(" ij: %p, age: %d\n", mat[i][j].humano, mat[i][j].humano->age);
    mat[x][y].humano = human_data();
    mat[x][y].humano->age = mat[i][j].humano->age;
    mat[x][y].humano->sick = mat[i][j].humano->sick;
    mat[x][y].humano->sick_time = mat[i][j].humano->sick_time;
    mat[x][y].humano->remaining_inmunity = mat[i][j].humano->remaining_inmunity;

    mat[x][y].occupied = true;
    mat[x][y].moved = true;
    printf("mat_pre: %d\n", mat[x][y].humano->age);
    mat[i][j].humano = NULL;
    /*mat[i][j].humano->age = 0;
    mat[i][j].humano->remaining_inmunity = 0;
    mat[i][j].humano->sick = 0;
    mat[i][j].humano->sick_time = 0;*/
    mat[i][j].occupied = 0;
    mat[i][j].moved = 0;
    printf("xy: %p, ij: %p\n", mat[x][y].humano, mat[i][j].humano);
    printf("mat_post: %d\n", mat[x][y].humano->age);
    //free(humanP);
    //mat[i][j].humano = NULL;
    pos[0] = x;
    pos[1] = y;
}

void infect(human* humanP, int infectiousness, int i, int j){
    for(int x = 0; x < DIM; x++){
        for(int y = 0; y < DIM; y++){
            /* Occupied, Not sick, not immune and adyacent patches*/
            if((mat[x][y].occupied) && !(mat[x][y].humano->sick) && (mat[x][y].humano->remaining_inmunity == 0) && ((x == i+1 && (y == j+1 || y == j-1)) || (x == i-1 && (y == j+1 || y == j-1)))){
                int rand_infect = rand() % 100;
                if(rand_infect < infectiousness){
                    //printf("UN GET SICK DESDE INFECT, i: %d, j: %d, x: %d, y: %d\n", i, j, x, y);
                    get_sick(mat[x][y].humano);
                }
            }
        }
    }
}

void hatch(){
    //printf("UN HATCH\n");
    int x = rand()%DIM;
    int y = rand()%DIM;
    while (mat[x][y].occupied == 1)
    {
        x = rand()%DIM;
        y = rand()%DIM;
    }
    mat[x][y].humano = human_data();
    mat[x][y].occupied = true;
    mat[x][y].moved = false;
    get_healthy(mat[x][y].humano);
    mat[x][y].humano->age = 1;
    count_turtles++;
}

void reproduce(int number_people, int chance_reproduce){
    int rand_reproduce = rand() % 100;
    if((count_turtles < carrying_capacity) && (rand_reproduce < chance_reproduce)){
        hatch(mat);
    }
}

void setup_turtles(human* humanP, int number_people){
    int sick = rand() % number_people;
    humanP->age = rand() % lifespan;
    humanP->sick_time = 0;
    humanP->remaining_inmunity = 0;
    get_healthy(humanP);
    printf("TORTUGA: %d\n", humanP->age);
    if(sick < (number_people/4 + 1)){
        printf("Se va a enfermar\n");
        get_sick(humanP);
    }
    else{
        printf("NOT SICK\n");
    }
}

void setup(int number_people, int infectiousness, int chance_recover, int duration, int sim_weeks){
    setup_constants();
    fill_mat();
    //results* resultsP = results_data();
    //printf("lifespan: %d\n", lifespan);
    sick_q = 0;
    char* fname = malloc(25*sizeof(char));
    fname = "threads.csv";
    write_file(fname);
    count_turtles = number_people;
    matriz_human(number_people);
    //printf("sale de matriz\n");
    limits a;
    a.x_inf = 0;
    a.y_inf = 0;
    a.x_sup = (DIM/2);
    a.y_sup = (DIM/2);
    a.number_people = count_turtles;
    limits b;
    b.x_inf = DIM/2;
    b.y_inf = 0;
    b.x_sup = DIM;
    b.y_sup = (DIM/2);    
    b.number_people = count_turtles;
    limits c;
    c.x_inf = 0;
    c.y_inf = (DIM/2);
    c.x_sup = (DIM/2);
    c.y_sup = DIM;
    c.number_people = count_turtles;
    limits d;
    d.x_inf = DIM/2;
    d.y_inf = DIM/2;
    d.x_sup = DIM;
    d.y_sup = DIM;
    d.number_people = count_turtles;
    pthread_t t1;
    pthread_create(&t1,NULL,update_global_variables,&a);
    pthread_t t2;
    pthread_create(&t2,NULL,update_global_variables,&b);
    pthread_t t3;
    pthread_create(&t3,NULL,update_global_variables,&c);
    pthread_t t4;
    pthread_create(&t4,NULL,update_global_variables,&d);
    void* r_av;
    void* r_bv;
    void* r_cv;
    void* r_dv;
    pthread_join(t1, &r_av);
    pthread_join(t2, &r_bv);
    pthread_join(t3, &r_cv);
    pthread_join(t4, &r_dv);
    results* r_a = (results *)r_av; 
    results* r_b = (results *)r_bv; 
    results* r_c = (results *)r_cv; 
    results* r_d = (results *)r_dv; 
    append_to_file(fname, 1, r_a->infected + r_b->infected + r_c->infected + r_d->infected, r_a->immune + r_b->immune + r_c->immune + r_d->immune, r_a->healthy + r_b->healthy + r_c-> healthy + r_d->healthy, count_turtles);
    //printf("llega aqui\n");
    
    for(int i = 0; i < (sim_weeks - 1); i++){
        printf("MATRIZ EN LA SEMANA: %d\n", i);
        print_mat(mat);
        //printf("primer for\n");
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied && !mat[x][y].moved){
                    //printf("DENTRO DE IF OCCUPIED\n");
                    get_older(x, y);
                    int pos[2]; // pos[0] = moved x, pos[1] = moved y
                    if(!died){
                        move(pos, mat[x][y].humano, x, y);
                    }
                    
                    /*if(mat[pos[0]][pos[1]].humano->sick){
                        printf("UN SICK TO RECOVER & INFECT\n");
                        recover_or_die(mat, duration, chance_recover, pos[0], pos[1]);
                        if(!died){
                            infect(mat[pos[0]][pos[1]].humano, mat, infectiousness, pos[0], pos[1]);
                        }
                        
                    }
                    else{
                        //printf("UN REPRODUCE\n");
                        reproduce(number_people, chance_reproduce, mat);
                    } */  
                }
                 
            }    
        }
        for(int x = 0; x < DIM; x++){
            //printf("segundo for\n");
            for(int y = 0; y < DIM; y++){
                //printf("tercer for\n");
                if(mat[x][y].occupied){
                    if(mat[x][y].humano->sick){
                        recover_or_die(duration, chance_recover, x, y);
                        if(!died){
                            infect(mat[x][y].humano, infectiousness, x, y);
                        }
                    }
                    else{
                        reproduce(number_people, chance_reproduce);
                    }
                    mat[x][y].moved = 0;
                    
                }
                    
            }    
        }
        a.number_people = count_turtles;
        b.number_people = count_turtles;
        c.number_people = count_turtles;
        d.number_people = count_turtles;
        pthread_create(&t1,NULL,update_global_variables,&a);
        pthread_create(&t2,NULL,update_global_variables,&b);
        pthread_create(&t3,NULL,update_global_variables,&c);
        pthread_create(&t4,NULL,update_global_variables,&d);   
        pthread_join(t1, &r_av);
        pthread_join(t2, &r_bv);
        pthread_join(t3, &r_cv);
        pthread_join(t4, &r_dv);
        r_a = (results *)r_av; 
        r_b = (results *)r_bv; 
        r_c = (results *)r_cv; 
        r_d = (results *)r_dv; 
        append_to_file(fname, i+2, r_a->infected + r_b->infected + r_c->infected + r_d->infected, r_a->immune + r_b->immune + r_c->immune + r_d->immune, r_a->healthy + r_b->healthy + r_c-> healthy + r_d->healthy, count_turtles);
        printf("ULTIMO PRINT\n");
        print_mat(mat);
        printf("\n\n");
        
        
    }

    //append_to_file(fname, week, humanP->sick, resultsP->infected, resultsP->immune);
}






#ifndef _WRITE_F_
#define _WRITE_F_


void write_file(char* filename);

void append_to_file(char* fname, int week, float infected, float immune, float healthy);

#endif
#include "write_file.h"
#include <stdio.h>
#include <string.h>

void write_file(char* fname){
    FILE* fpointer = fopen(fname, "w");
    fprintf(fpointer, "Week, Sick, Immune, Healthy\n");
    fclose(fpointer);
}

void append_to_file(char* fname, int week, float infected, float immune, float healthy){
    
    FILE* fpointer = fopen(fname, "a");
    
    fprintf(fpointer, "%d, %f, %f, %f\n", week, infected, immune, healthy);
   
    fclose(fpointer);
}
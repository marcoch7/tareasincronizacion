#include "part1.h"
#include "write_file.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>



int main(int argc, char const *argv[]){
    unsigned int number_people, infectiousness, chance_recover, duration, sim_weeks;
    printf("Enter number_people: ");
    scanf("%d", &number_people); 
    printf("\nEnter infectiousness: ");
    scanf("%d", &infectiousness); 
    printf("\nEnter chance_recover: ");
    scanf("%d", &chance_recover);  
    printf("\nEnter virus duration: ");
    scanf("%d", &duration);  
     printf("\nEnter simulation duration: ");
    scanf("%d", &sim_weeks);  
  

    patch mat[DIM][DIM];
    for (int i = 0; i < DIM; i++)
    {
        for (int j = 0; j < DIM; j++)
        {
            mat[i][j].humano = NULL;
            mat[i][j].occupied = false;
        }
        
    }
       
    setup(number_people, infectiousness, chance_recover, duration, mat, sim_weeks);        // jumps to this function, located at read.c

    /*matriz_human(number_people, mat);
    print_mat(mat);*/

    return 0;
}
